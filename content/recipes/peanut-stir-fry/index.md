---
layout: recipe
date: 2021-03-24T14:44:11Z
draft: false
title: "Peanut Stir Fry" # The title of your awesome recipe
blurb: This quick and easy sauce is so tasty! Whenever I make this meal I always do spiralised courgette and carrots, and often I use some sort of chicken substitute but it's a stir fry, so make it with whatever you like! # A quick description of the recipe
image: peanut-stir-fry.jpg # Name of image in recipe bundle
imagecredit: # URL to image source page, website, or creator
YouTubeID: # The F2SYDXV1W1w part of https://www.youtube.com/watch?v=F2SYDXV1W1w
authorName: # Name of the recipe/article author
authorURL: # URL of their home website
sourceName: allrecipes user Bonnie Gertz # Name of the source website
sourceURL: https://www.allrecipes.com/recipe/239947/thai-peanut-stir-fry-sauce/ # Actual URL of the recipe itself
category: "Dinner" # The type of meal or course your recipe is about. For example: "dinner", "entree", or "dessert".
cuisine: "Indonesian" # The region associated with your recipe. For example, "French", Mediterranean", or "American".
tags: # You don't have to have 3, feel free to have 10, 1, or none
  - Quick
  - Easy
  - Peanut Butter
  - Vegan
  - Main
  - Stir Fry
serves: 2-3
prepTime: 10
cookTime: 15

ingredients:
- SAUCE
- 2 tbsp red wine vinegar
- 2 tbps soy sauce
- 3 tbps crunchy peanut butter
- 1.5 tbps brown sugar
- 1 tsp garlic powder
- 1/2 tsp cayenne pepper
- 1/8 tsp ground ginger
- NOT SAUCE
- 1 Courgette
- 2 Carrots
- 1 Pepper
- Fake Chicken pieces
- Noodles (Buckwheat = vegan)

directions:
- Boil the kettle. Fill a pot of boiling water for the **noodles** (save some water for...)
- Slow cook all of the SAUCE ingredients in a pan. Low heat or the soy burns. Add a splash of hot water to loosen it up a little. It's going to reduce back down as it cooks, while you do the veg.
- You need to stir this semi-regularly and make sure to add a bit more water when it's needed. Don't add too much though, the residual water from the veg and noodles is going to water it down at the end.
- Get a frying pan/wok with oil up to a very high heat.
- (Optionally) Spiralise **1 courgette** and **2 carrots**. Slice **1 pepper** into thin strips.
- Throw on all the veg and **chicken**. Stir constantly.
- Get the noodles into the boiling water and cook to instruction.
- When the noodles are done, drain them and rinse them with cold water. Add them to the veg and mix it all up.
- Cover the veg/noodles with your sauce. Take it off the heat and mix really well. Serve!
---
