---
layout: recipe
date: 2021-05-06T10:35:26+01:00
draft: false
title: "Pasta Bake" # The title of your awesome recipe
blurb: This pasta bake is so filthy, the pesto and chunks of mozzerela are what really push it over the edge. I suggest using my [tomoato sauce recipe](https://food.pauloshea.ie/recipes/tomato-sauce/) if you need one! # A quick description of the recipe
image: pasta-bake.jpg # Name of image in recipe bundle
imagecredit: # URL to image source page, website, or creator
YouTubeID: # The F2SYDXV1W1w part of https://www.youtube.com/watch?v=F2SYDXV1W1w
authorName: # Name of the recipe/article author
authorURL: # URL of their home website
sourceName: BBC Food # Name of the source website
sourceURL: https://www.bbc.co.uk/food/recipes/vegetable_pasta_bake_15082 # Actual URL of the recipe itself
category: Dinner # The type of meal or course your recipe is about. For example: "dinner", "entree", or "dessert".
cuisine: Italian # The region associated with your recipe. For example, "French", Mediterranean", or "American".
tags: # You don't have to have 3, feel free to have 10, 1, or none
  - Pasta
  - Main
  - Pot Luck
  - Vegetarian
  - Easy
  - Bake
  - Italian
serves: 4
prepTime: 10
cookTime: 40

ingredients:
- Tomato Sauce (https://food.pauloshea.ie/recipes/tomato-sauce/)
- 1 red onion
- 1 red and 1 yellow pepper
- 1 aubergine
- 1 courgette
- olive oil
- salt and pepper
- 300g pasta
- 150g mozzerela
- 2-3 tbsp pesto
- grated mozzerela
- grated pecorino
- 
directions:
- Make your **tomato sauce**, or use one from a jar. I suggest using my [tomoato sauce recipe](https://food.pauloshea.ie/recipes/tomato-sauce/)!
- Preheat the oven to 190C and put a large frying/griddle pan on the hob on a high heat
- Slice **1 red onion**, chop **1 red and 1 yellow pepper** into chunks, and chop **1 aubergine** and **1 courgette** into slices
- Put the peppers, courgette and onions into a  deep baking dish and cover generously in **olive oil**. Toss and season with **salt and pepper**. Cook for 20-30 minutes.
- Meanwhile lightly cover both sides of the aubergine slices in olive oil. Put them on the pan and fry on both sides til brown. You'll probably need to do them in batches. Once they're all done, they can join the rest of the veg in the baking dish.
- With 10 minutes to go on the veg, cook **300g pasta** to packet instructions
- Add your sauce to the veg. Drain the pasta and stir through the veg.
- Break **150g mozzerela** into chunks. Add it and **2-3 tbsp pesto** to the mix.
- Top with **grated mozzerela** and **grated pecorino** (veggie hard cheese) and melt under the grill
---
