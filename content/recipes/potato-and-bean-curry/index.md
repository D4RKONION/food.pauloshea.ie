---
layout: recipe
date: 2021-04-03T14:27:52+01:00
draft: false
title: "Potato and Bean Curry" # The title of your awesome recipe
blurb: This delicious curry is really handy if you don't have many perishables in stock. Just some onion, tomato and potatoes basically! Also Ginger paste is a life saver # A quick description of the recipe
image: potato-and-bean-curry.jpg # Name of image in recipe bundle
imagecredit: # URL to image source page, website, or creator
YouTubeID: # The F2SYDXV1W1w part of https://www.youtube.com/watch?v=F2SYDXV1W1w
authorName: # Name of the recipe/article author
authorURL: # URL of their home website
sourceName: Happy Pear P???  # Name of the source website
sourceURL: # Actual URL of the recipe itself
category: "Dinner" # The type of meal or course your recipe is about. For example: "dinner", "entree", or "dessert".
cuisine: Indian # The region associated with your recipe. For example, "French", Mediterranean", or "American".
tags: # You don't have to have 3, feel free to have 10, 1, or none
  - Quick
  - Easy
  - Curry
  - Vegan
  - One Pot
  - Main
  - Indian
serves: 8
prepTime: 5
cookTime: 50

ingredients:
- 1 tin Kidney Beans
- 1 Onion
- 1 Tomato
- 2 Cloves Garlic
- 1 tbsp Ginger
- 2 Large Potatoes
- 1/2 tsp Tumeric
- 1 tbsp Curry Powder
- Corriander

directions:
- Heat some oil in a pan on high.
- Drain **1 tin kidney beans** and rinse
- Peel and finely chop an **onion**, finely dice the **a tomato**, peel and crush **2 cloves of garlic** and a **1tbsp grated ginger**.
- Then turn down the heat and add the Tomato and Onion. Simmer for 5 mins with lid on.
- Add the garlic and ginger and simmer for 5 more minutes. While that’s going, cut **2 large potatoes** lengthways into wedges (8 per potato) and boil kettle.
- Add **1/2 tsp of tumeric** and **1 tbsp Curry powder** to the tomatoes/onions, stir well. Simmer for 3 mins on low/medium heat, stir frequently. Add a little water if sticking.
- Add the beans and potatoes and some salt and 200ml of boiling water and stir well. Bring to the boil then simmer for 30/40 mins, until consistency isn't too watery. Stir every 10 mins or so.
- Add **Corriander** just before the end 
---
