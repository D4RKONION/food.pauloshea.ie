---
layout: recipe
date: 2021-04-03T13:33:21+01:00
draft: false
title: "Aubergine Parmigiana" # The title of your awesome recipe
blurb: This one's basically just aubergine baked in a tomato sauce covered in bread and cheese. Need I say any more?!   # A quick description of the recipe
image: aubergine-parmigiana.jpg # Name of image in recipe bundle
imagecredit: # URL to image source page, website, or creator
YouTubeID: # The F2SYDXV1W1w part of https://www.youtube.com/watch?v=F2SYDXV1W1w
authorName: # Name of the recipe/article author
authorURL: # URL of their home website
sourceName: Jamie Oliver # Name of the source website
sourceURL: https://www.jamieoliver.com/recipes/vegetables-recipes/aubergine-parmigiana-melanzane-alla-parmigiana/ # Actual URL of the recipe itself
category: "Dinner" # The type of meal or course your recipe is about. For example: "dinner", "entree", or "dessert".
cuisine: "Italian" # The region associated with your recipe. For example, "French", Mediterranean", or "American".
tags: # You don't have to have 3, feel free to have 10, 1, or none
  - Quick
  - Easy
  - Vegetarian
  - Bake
serves: 2-3
prepTime: 5
cookTime: 25 (if the sauce is premade)

ingredients:
- 1/2 batch of [⚙️Tomato sauce](https://food.pauloshea.ie/recipes/tomato-sauce/)
- 2 courgettes
- 1 aubergine
- salt
- olive oil
- cheddar
- breadcrumbs or bread
- vegetarian hard cheese

directions:
- You'll need half a batch of **tomato sauce** for this recipe. Cook your own alongside this (one can of chopped tomatoes worth) or [try mine](https://food.pauloshea.ie/recipes/tomato-sauce/)!
- Slice **2 courgettes** and **1 aubergine**, put them in a bowl, sprinkle with a good bit of **salt** and toss. Leave them aside while you...
- Get a griddle pan or frying pan up to a very high heat.
- Add some **olive oil** to the bowl and toss so everything is well coated.
- Start drying the veg in the pan, flipping half way through when just browning.
- Into an oven dish, layer the aubergine on the bottom and the courgette on top.
- Grate **cheddar** and **bread** over the top. Bake for about 5-10 minutes and then switch a grill to brown the bread and melt the cheese if needed.
- Serve with a **vegetarian hard cheese**

---
