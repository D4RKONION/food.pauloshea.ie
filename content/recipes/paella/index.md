---
layout: recipe
date: 2021-03-30T14:50:56+01:00
draft: false
title: "Paella" # The title of your awesome recipe
blurb: "Okay, so Paella might be a bit of a misnomer here, but it uses Paella rice and I'm never gonna eat actual Paella anyway so whatever! Alls I know is this is quick, easy and tasty" # A quick description of the recipe
image: paella.jpg # Name of image in recipe bundle
imagecredit: # URL to image source page, website, or creator
YouTubeID: # The F2SYDXV1W1w part of https://www.youtube.com/watch?v=F2SYDXV1W1w
authorName: # Name of the recipe/article author
authorURL: # URL of their home website
sourceName: # Name of the source website
sourceURL: # Actual URL of the recipe itself
category: dinner # The type of meal or course your recipe is about. For example: "dinner", "entree", or "dessert".
cuisine: Spanish # The region associated with your recipe. For example, "French", Mediterranean", or "American".
tags: # You don't have to have 3, feel free to have 10, 1, or none
  - Easy
  - Quick
  - One Pot
  - Vegan
serves: 2-3
prepTime: 5
cookTime: 30

ingredients:
- 1 Carrot
- 1 Pepper
- 1 Onion
- 1 clove Garlic
- Chicken Alternative (e.g. quorn)
- 1 tbsp Parsley
- 1 tbsp Paprika
- 1 tbsp Tomato Puree 
- 1 tbsp Stock
- 120g Paella Rice
- 400ml Boiling Water
- Handful or 2 of Frozen Peas

directions:
- Chop **1 carrot**, **1 pepper**, **1 onion**, **1 clove garlic** and some **chicken alternative** (optional).
- Heat oil, add carrot, garlic, onion and chicken. Immediately add **1 tbsp parsley** and **1 tbsp paprika**. Cook for ~5mins.
- Add the peppers. Cook for ~5mins.
- Add **1 tbsp tomato puree** and stir through. Add **1 tbsp veg stock powder** and stir through. Add **120g paella rice** and mix.
- Add **400ml boiling water**. Bring to the boil and then leave to simmer (heat ~1) with lid on for 15mins. Check periodically to add more water if necessary.
- Add a **handful or 2 of frozen peas**. Put lid back on and let cook for 5 mins (don’t forget to add a little water if necessary
---
