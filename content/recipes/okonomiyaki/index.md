---
layout: recipe
date: 2021-03-15T11:10:38Z
draft: false
title: "Okonomiyaki" # The title of your awesome recipe
blurb: "I love Okonomiyaki so much! お好み焼き literally means 'Whatever you want - fried', so experiment with the fillings! The [Vegetarian Bulldog](http://tonkatsu.bulldog.jp/images/bulldogsauce/img_22.png) sauce is absolutely key, don't skip on it! You can get it any almost asian store."
image: okonomiyaki.jpg # Name of image in recipe bundle
imagecredit: # URL to image source page, website, or creator
YouTubeID: # The F2SYDXV1W1w part of https://www.youtube.com/watch?v=F2SYDXV1W1w
authorName: Jamie Oliver # Name of the recipe/article author
authorURL: # URL of their home website
sourceName: Save With Jamie P? # Name of the source website
sourceURL: # Actual URL of the recipe itself
category: Dinner # The type of meal or course your recipe is about. For example: "dinner", "entree", or "dessert".
cuisine: "Japanese" # The region associated with your recipe. For example, "French", Mediterranean", or "American".
tags: # You don't have to have 3, feel free to have 10, 1, or none
  - Vegetarian
  - Egg
  - Quick
  - Easy
  - Japanese
  - Main
serves: 1-2
prepTime: 5
cookTime: 20

ingredients:
- 4 eggs
- 100g Flour
- 1/4 an Onion
- A few 1cm cubes of Tofu
- 2 mushrooms
- Some chopped florettes of brocolli
- Bulldog sauce
- Salt and Pepper
- Scallions

directions:
- Crack **4 eggs** into a bowl and whisk. Whisk in **100g flour** a small amount at a time until smooth. Set aside.
- Dice a **1/4 onion**, chop the **mushrooms** into small chunks and the **brocolli** into very tiny floettes. You don't need a huge amount of filling, so be conservative!
- Heat a frying pan with a small amount of oil on a high heat.
- Fold the chopped veg and **tofu** into the egg mixture.
- Once the pan is very hot, pour the mixture in. Cook until brown on one side.
- Flip the Okonomiyaki onto a plate. Get the pan nice and hot again with a bit more oil and slide the raw side back into the pan.
- While the other side cooks, spread the **[Vegetarian Bulldog](http://tonkatsu.bulldog.jp/images/bulldogsauce/img_22.png)** onto the cooked side. Use a brush or spoon to spread it out.
- Season with **salt and pepper** and serve with **mayonnaise** spread in the classic pattern over the top and some chopped **scallions**.

---
