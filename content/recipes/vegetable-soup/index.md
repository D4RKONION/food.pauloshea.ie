---
layout: recipe
date: 2021-08-09T14:35:35+01:00
draft: false
title: "Vegetable Soup" # The title of your awesome recipe
blurb: A hearty, basic vegetable soup that's perfect for a cold winter's day! Leave out the cream to keep it vegan # A quick description of the recipe
image: vegetable-soup.jpg # Name of image in recipe bundle
imagecredit: # URL to image source page, website, or creator
YouTubeID: # The F2SYDXV1W1w part of https://www.youtube.com/watch?v=F2SYDXV1W1w
authorName: # Name of the recipe/article author
authorURL: # URL of their home website
sourceName: # Name of the source website
sourceURL: # Actual URL of the recipe itself
category: "dinner" # The type of meal or course your recipe is about. For example: "dinner", "entree", or "dessert".
cuisine: "Everywhere" # The region associated with your recipe. For example, "French", Mediterranean", or "American".
tags: # You don't have to have 3, feel free to have 10, 1, or none
  - Freezes Well
  - Main
  - Vegan
  - Soup
serves: 6
prepTime: 15
cookTime: 45

ingredients:
- 1 leek
- 4 carrots
- 1 red onion
- 2 sticks celery
- olive oil
- 2 medium sweet potatoes
- butternut squash
- 2 litres of water
- ¾ tbsp fennel seeds
- 1 chilli (optional)
- 4tsp veg stock
- cream

directions:
- Wash and finely slice **1 leek**, and slice **4 carrots**, **1 red onion** and **2 sticks celery**
- Heat a large pot on medium heat with a little **olive oil**. Once hot, add the above veg. Cook until soft and sweet (10-15 mins)
- Peel **2 medium sweet potatoes**, and peel and prep the **butternut squash**. Chop them into chunks.
- Boil **2 litres of water** in the kettle.
- Once the veg are soft and cooked, add the potato and squash, **¾ tbsp fennel seeds**, **1 chilli (optional)**, **4tsp veg stock**. Add the 2 litres of water. Bring it all to the boil and reduce to a simmer. Cook for 40 minutes or longer.
- Blitz it into a smooth soup. Serve with **cream**/whatever
---
