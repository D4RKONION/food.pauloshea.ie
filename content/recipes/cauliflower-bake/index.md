---
layout: recipe
date: 2021-08-09T14:35:35+01:00
draft: false
title: "Cauliflower Bake" # The title of your awesome recipe
blurb: This one's a lot of work for what is essentially a side dish, but sometimes we eat it as a main. It's so filthy mmmm # A quick description of the recipe
image: cauliflower-bake.jpg # Name of image in recipe bundle
imagecredit: # URL to image source page, website, or creator
YouTubeID: # The F2SYDXV1W1w part of https://www.youtube.com/watch?v=F2SYDXV1W1w
authorName: Save with Jamie P62 # Name of the recipe/article author
authorURL: # URL of their home website
sourceName: # Name of the source website
sourceURL: # Actual URL of the recipe itself
category: "dinner" # The type of meal or course your recipe is about. For example: "dinner", "entree", or "dessert".
cuisine: "British" # The region associated with your recipe. For example, "French", Mediterranean", or "American".
tags: # You don't have to have 3, feel free to have 10, 1, or none
  - British
  - Main
  - Side
  - Bake
  - Vegetarian
  - Long
  - Roux
serves: 4
prepTime: 10
cookTime: 80

ingredients:
- Head of Broccoli
- 1000g Cauliflower
- 50g Butter
- Three Cloves of Garlic
- 50g Flour
- 500ml Milk
- 150g of Cheddar
- Salt and Pepper
- Slice of Bread
- Salt
- Pepper
- Thyme

directions:
- Chop a **head of broccoli** into small florets. We’ll chop the cauliflower later
- Heat a medium sized pan on a medium heat. Add **50g butter**
- Finely slice **three cloves of garlic** and put them in the butter. Stir til melted and the garlic somewhat cooked
- Stir in **50g flour** gradually until an even paste
- Gradually stir in **500ml milk**, until smooth (and it’ll be a little watery)
- Add the broccoli to the sauce and simmer for about 20 minutes or until they’re mush
- While the sauce simmers preheat the oven to 180c
- Chop the **1000g cauliflower** head into bite-size florets and lay them in a baking dish
- When the broccoli is mush, pulse it with a hand mixer (loosen with a splash of milk)
- Grate in **150g of cheddar** and stir. **Salt and Pepper** to taste
- Grate a **slice of bread** into breadcrumbs and add **salt**, **pepper** and **thyme** and a splash of oil
- Pour the sauce over the cauliflower. Grate cheese over the sauce. Put the breadcrumb mix over the cheese. 
- Bake for 45-60 minutes or until the breadcrumbs are golden brown

---
