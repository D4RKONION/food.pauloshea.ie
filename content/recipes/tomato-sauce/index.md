---
layout: recipe
date: 2021-04-03T12:55:44+01:00
draft: false
title: "⚙️Tomato Sauce" # The title of your awesome recipe
blurb: I use this Neapolitan sauce in most recipes that require a tomato-ey sauce, from pizza to pasta to parmigiana. I usually go a little bit mad on the oregano but you should experiment and find the balance you like  # A quick description of the recipe
image: tomato-sauce.jpg # Name of image in recipe bundle
imagecredit: https://buzzfeed.com/ # URL to image source page, website, or creator
YouTubeID: # The F2SYDXV1W1w part of https://www.youtube.com/watch?v=F2SYDXV1W1w
authorName:  # Name of the recipe/article author
authorURL: # URL of their home website
sourceName: Basics with Babish Ep 1 # Name of the source website
sourceURL: https://www.youtube.com/watch?v=Upqp21Dm5vg # Actual URL of the recipe itself
category: Component # The type of meal or course your recipe is about. For example: "dinner", "entree", or "dessert".
cuisine: Italian # The region associated with your recipe. For example, "French", Mediterranean", or "American".
tags: # You don't have to have 3, feel free to have 10, 1, or none
  - Component
  - Sauce
  - Easy
  - Long
  - Freezes Well
serves: "?"
prepTime: 5
cookTime: 70

ingredients:
- 3 cloves Garlic
- 1 Onion
- Olive Oil
- 1 tsp Red Pepper Flake
- 1.5 tsp Dried Oregano
- 1 tbsp Tomato Puree
- 2 cans Chopped Tomatoes
- ½ tsp Basil
- 1 Bayleaf
- 1.5 tbsp Butter
- Salt & Pepper

directions:
- Mince **3 cloves garlic** and finely chop a **1 yellow onion**
- Saute the garlic in a good bit of **olive oil** in a pot. Add some **red pepper flake** and **dried oregano** for about 1 minute
- Add the onion and sweat it down for a few minutes.
- Add a **1 tbsp of tomato puree** and **2 cans tomaotes**. 
- Stir in **½ tsp of basil** and a **bayleaf**, bring to a bare simmer and cook for an hour
- Add **1.5tbsp of butter** and some **salt and pepper**
---
