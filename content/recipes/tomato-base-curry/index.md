---
layout: recipe
date: 2021-08-04T13:32:21+01:00
draft: false
title: "Tomato Base Curry" # The title of your awesome recipe
blurb: This is an amazing basic curry which you can serve with rice or naan. I usually add some vegetables or potato too. You could leave out the cream to make it vegan # A quick description of the recipe
image: tomato-base-curry.jpg # Name of image in recipe bundle
imagecredit: https://placekitten.com/600/800 # URL to image source page, website, or creator
YouTubeID: # The F2SYDXV1W1w part of https://www.youtube.com/watch?v=F2SYDXV1W1w
authorName: # Name of the recipe/article author
authorURL: # URL of their home website
sourceName: foodandwine.com # Name of the source website
sourceURL: https://www.foodandwine.com/recipes/basic-indian-tomato-curry # Actual URL of the recipe itself
category: dinner # The type of meal or course your recipe is about. For example: "dinner", "entree", or "dessert".
cuisine: Indian # The region associated with your recipe. For example, "French", Mediterranean", or "American".
tags: # You don't have to have 3, feel free to have 10, 1, or none
  - One Pot
  - Easy
  - Indian
  - Freezes Well
serves: 
prepTime: 15
cookTime: 45

ingredients:
- 1/4 cup of oil or ghee
- 1 Onion
- 1 tsp Salt
- 3 tbsp ginger
- 4 cloves garlic crushed
- 1 tsp garam masal
- 1 tsp tumeric
- 1 tsp corriander
- 1 tsp cumin
- 1 tsp cayenne (or less)
- 4 cardamon pods seeds
- 1 tin chopped tomatoes
- 1/4 cup heavy cream

directions:
- Heat the **oil** on a medium heat in a large frying pan.
- Chop **1 onion** and fry it with **1 tsp salt** until the onions are brown (8 mins)
- Add **3 tbsp ginger** and **4 cloves crushed garlic** for 1 minute
- Add **1 tsp garam masal**, **1 tsp tumeric**, **1 tsp corriander**, **1 tsp cumin**, **1 tsp cayenne (or less)**, **4 cardamon pods seeds**. Cook for 30 seconds
- Add **1 tin chopped tomatoes** and stir.
- Add **1/2 cup of water**, and reduce to a simmer for 15 minutes.
- Take off the heat and allow to cool before adding **1/4 cup heavy cream** 
---
