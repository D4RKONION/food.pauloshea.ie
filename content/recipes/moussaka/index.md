---
layout: recipe
date: 2021-03-17T23:35:57Z
draft: false
title: "Moussaka" # The title of your awesome recipe
blurb: This is great either as a meal on it's own or as a side dish for a roast. It's not hard to make but there's a lot of different components to cook at the same time so I often get someone to help me with it. # A quick description of the recipe
image: moussaka.jpg # Name of image in recipe bundle
imagecredit: # URL to image source page, website, or creator
YouTubeID: # The F2SYDXV1W1w part of https://www.youtube.com/watch?v=F2SYDXV1W1w
authorName: # Name of the recipe/article author
authorURL: # URL of their home website
sourceName: ADD THE BOOK NAME AND PAGE # Name of the source website
sourceURL: # Actual URL of the recipe itself
category: Dinner # The type of meal or course your recipe is about. For example: "dinner", "entree", or "dessert".
cuisine: Greek # The region associated with your recipe. For example, "French", Mediterranean", or "American".
tags: # You don't have to have 3, feel free to have 10, 1, or none
  - Main
  - Side
  - Bake
  - Long
  - Many hands
  - Roux
  - Vegetarian
serves: 2 as a main
prepTime: 10
cookTime: 60

ingredients:
- 1 Aubergine
- 500g (New) Potatoes
- 2 Red Onions
- 300g (Cherry) Tomatoes
- 300ml Milk
- 30g Flour
- 30g Butter
- 1 Clove Garlic

directions:
- Boil the kettle. Wash your **500g potatoes** and boil them for 20 minutes.
- Put the oven on 180 C. Put a large frying pan or griddle on the highest heat.
- Chop **2 red onions** into slices and **300g tomatoes** (1/2 Cherry Tomatoes or that size for regular). Put them in an oven dish with some olive oil and shake em up. Then get them in the oven, til they're well cooked (15-20 minutes).
- Chop an **aubergine** into 1cm thick slices and lightly cover both sides in olive oil. Put them on the pan and fry on both sides til brown. You'll probably need to do them in batches.
- Melt **30g butter** in a pan. Crush **1 clove garlic** into it and fry for 30 seconds.
- Add your **30g flour** gradually and whisk. Cook for a few minutes until the flour smell is gone. You should be left with a paste.
- Gradually whisk **300ml Milk** into the paste. It may seem a litte wattery towards the end. Don't panic, this will thicken. Bring to a simmer and then get the temperature down to a bare simmer. Cook until thick (15-20 minutes). Stir often to avoid film/skin forming.
- When the potatoes are cooked, get a thick-based pan on a high heat and put the potatoes in. Use a masher to mash them down about halfway (i.e. flatten them a little!). Allow the potatoes to fry a little in the oil.
- Assemble everything in an oven dish. Potatoes on the bottom, onion and tomatoes next, layer your aubergine on top and pour over the roux. Put it in an oven on a high heat til the top has browned.
- Season and enjoy!
---
