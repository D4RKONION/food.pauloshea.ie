---
layout: recipe
date: 2021-03-16T22:39:43Z
draft: false
title: "Peanut Butter Soup" # The title of your awesome recipe
blurb: "I know what you're thinking: three heaped tablespoons of peanut butter seems like a lot. Well, ask yourself: do you want this to taste like peanut butter or do you want it taste like vegetable stock?? Yeah that's what I thought." # A quick description of the recipe
image: peanut-butter-soup.jpg # Name of image in recipe bundle
imagecredit:  # URL to image source page, website, or creator
YouTubeID: # The F2SYDXV1W1w part of https://www.youtube.com/watch?v=F2SYDXV1W1w
authorName: # Name of the recipe/article author
authorURL: # URL of their home website
sourceName: YSAC Ep27 # Name of the source website
sourceURL: https://www.youtube.com/watch?v=PHdPLQdjsfE # Actual URL of the recipe itself
category: Dinner # The type of meal or course your recipe is about. For example: "dinner", "entree", or "dessert".
cuisine: West African # The region associated with your recipe. For example, "French", Mediterranean", or "American".
tags: # You don't have to have 3, feel free to have 10, 1, or none
  - Peanut Butter
  - Soup
  - Spicy
  - One Pot
  - Main
serves: 2
prepTime: 10
cookTime: 30-50

ingredients:
- 1 Large Carrot
- Medium Head of Broccoli
- 1/2 Red Onion
- 1 Clove of Garlic
- 400ml Broth ([2 tsp powder](https://digitalcontent.api.tesco.com/v2/media/ghs/ae73f9e4-85b3-40f5-b8d2-c8deb228bf1d/snapshotimagehandler_157391959.jpeg?h=540&w=540))
- 400ml Can of Chopped Tomatoes
- 1 Can of Chickpeas
- 3 tbsp peanut butter
- 2-3 tsp hot sauce
- 1/2 tsp Garlic granules/powder
- 1.5 tsp Brown Sugar
- Salt and Pepper

directions:
- Shred **1 large carrot** in a grater. Chop a **medium sized head of broccoli** into small pieces.
- Boil the kettle.
- In a large saucepan, heat **2 tbsp olive oil** over medium heat. Add **1/2 a red onion** diced to the pan and saute till turning brown (about 3-4 minutes) with **one clove of garlic** crushed or minced.
- While the onion cooks, make **400ml vege broth** ([2 tsp powder](https://digitalcontent.api.tesco.com/v2/media/ghs/ae73f9e4-85b3-40f5-b8d2-c8deb228bf1d/snapshotimagehandler_157391959.jpeg?h=540&w=540)).
- Drain **1 can of chopped tomatoes**. Drain and rinse **1 Can of Chickpeas**.
- Add the broth, broccoli, carrots, tomatoes, and chickpeas to the pot and mix. Add **3 tbsp peanut butter**. And **2-3 tsp hot sauce**. I use siracha, but anything hot and red). 
- Add a sprinkle of **garlic granules/powder**. Add **1.5 tsp brown sugar**. Season with **salt and pepper**.
- Mix it thoroughly, get it to a boil and then bring the heat down to maintain a gentle simmer while the lid is on. Leave it like this for about 20 minutes, stirring occasionally.
- Take the lid off and adjust the taste. Sometimes it needs a bit more peanut butter or sugar.
- If you're in a rush, you can eat it now, but I like to let it cook for another 15-20 minutes first.
- Serve with bread and butter!
---
