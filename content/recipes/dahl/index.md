---
layout: recipe
date: 2021-04-29T15:09:27+01:00
draft: false
title: "Dahl" # The title of your awesome recipe
blurb: "This is very like the chickpea curry but with less chickpeas... and more lentils... Just as tasty though!" # A quick description of the recipe
image: dahl.jpg # Name of image in recipe bundle
imagecredit: # URL to image source page, website, or creator
YouTubeID: # The F2SYDXV1W1w part of https://www.youtube.com/watch?v=F2SYDXV1W1w
authorName: # Name of the recipe/article author
authorURL: # URL of their home website
sourceName: Happy Pear P10 # Name of the source website
sourceURL: # Actual URL of the recipe itself
category: Dinner # The type of meal or course your recipe is about. For example: "dinner", "entree", or "dessert".
cuisine: Indian # The region associated with your recipe. For example, "French", Mediterranean", or "American".
tags: # You don't have to have 3, feel free to have 10, 1, or none
  - Indian
  - Main
  - One Pot
  - Vegan
  - Pot Luck
serves: 4
prepTime: 5
cookTime: 35 to 45

ingredients:
- Brown Rice
- 1 Red Onion
- 2 Cloves Garlic
- 1/2 thumb size Ginger
- Oil
- 1 Courgette
- 2 Medium Tomatoes
- 250g Red Lentils
- 1.5 tsp of Salt
- 1 tsp of Ground Cumin
- 1/2 tsp of Cayenne
- 1/2 tsp Tumeric
- 1.5 tsp of Curry Powder
- 1/2 tsp of Black Pepper
- 1.5 tablespoon of Soy Sauce
- 900ml Boiling Water
- Fresh Corriander

directions:
- Put on some **brown rice** (or don't! You can enjoy this with pita)
- Peel and finely slice **1 red onion**, **2 cloves garlic** and **1/2 thumb size piece ginger**. Saute the onions, garlic and ginger with a good amount of **oil** in a pot on a high heat for 5 minutes. 
- Meanwhile, cut **1 courgette** into bite size pieces and roughly chop **2 medium tomatoes**.
- When the onions are soft, add courgette and tomatoes and **1/3 tsp salt**. Cover and cook for at least 10 minutes on a low heat, though I tend to go for 15-20 if I have time.
- Meanwhile, rinse **250g Red Lentils** thoroughly and put on the kettle
- After your veg has cooked add the lentils, **3/4 tsp salt**, **1 tsp ground cumin**, **1/2 tsp cayenne**, **1/2 tsp tumeric**, **1.5 tsp curry powder**, **1/2 tsp black pepper**, **1.5 tablespoon soy sauce** and **900ml boiling water**.
- Bring to the boil. Reduce to low heat and simmer for 25 minutes (longer if need right texture). Stir regularly to stop the lentils from sticking
- Add some **fresh corriander** at the end (dried is fine too).

---