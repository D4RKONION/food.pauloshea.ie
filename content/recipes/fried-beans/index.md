---
layout: recipe
date: 2021-08-04T12:57:21+01:00
draft: false
title: "Fried Beans" # The title of your awesome recipe
blurb: This is quick, versatile and you can mix it up with as many or as few veggies as you want. I always put these in wraps with loads of cheese and homemade guac! # A quick description of the recipe
image: fried-beans.jpg # Name of image in recipe bundle
imagecredit: # URL to image source page, website, or creator
YouTubeID: # The F2SYDXV1W1w part of https://www.youtube.com/watch?v=F2SYDXV1W1w
authorName: # Name of the recipe/article author
authorURL: # URL of their home website
sourceName: You Suck At Cooking 107 # Name of the source website
sourceURL: https://www.youtube.com/watch?v=SiYZnPk_Lwg # Actual URL of the recipe itself
category: dinner # The type of meal or course your recipe is about. For example: "dinner", "entree", or "dessert".
cuisine: Mexican # The region associated with your recipe. For example, "French", Mediterranean", or "American".
tags: # You don't have to have 3, feel free to have 10, 1, or none
  - Quick
  - Easy
  - Mexican
  - Main
  - One Pot
  - Vegan
serves: 3-4
prepTime: 5
cookTime: 20

ingredients:
- 100g Rice
- 3tbsp Oil
- 1 Red Onion
- 1 Chilli or Jalapeno
- 1 Red Pepper
- 2 Cloves of Garlic
- 1 tin Kidney/Black Beans
- 1.5tbsp Cumin
- 1 tsp Chilli Powder
- ¾ tsp salt
- ½ cup water


directions:
- Defrost the **wraps** if you're gonna use wraps and you keep your spare wraps in the freezer. This is mostly just a reminder for me, soz
- Make **100g of rice**.
- Heat **3 tbsp of oil** in a big frying pan to medium. Dice **almost a whole 1 red onion** (use rest for guac) and **half a japapeno or chilli**. Chop **1 red pepper** and **2 cloves of garlic**.
- When the oil is hot, add the onion, chilli and garlic. Fry for a minute, then add the pepper. Fry for 7 minutes.
- Drain some **kidney/black beans**. After about 8 minutes, add the beans, **1.5 tbsps cumin**, **1 tsp chilli powder** and **¾ tsp salt**.
- Stir, don’t panic, things get dry. Add **½ cup water**, mix and cook on a medium heat until everything is thick.
- Make **guac**, **grate cheese**, get **hot sauce** ready (if you're doing wraps, which you should)
- Add the rice to the beans, bit by bit, stirring all the time. Or serve on top of the rice.

---
