---
layout: recipe
date: 2021-08-09T14:35:35+01:00
draft: false
title: "Lasagna" # The title of your awesome recipe
blurb: The white sauce component of this one may cook strange but it's so good. I suggest using my [tomato sauce recipe](https://food.pauloshea.ie/recipes/tomato-sauce/) if you need one!  # A quick description of the recipe
image: lasagna.jpg # Name of image in recipe bundle
imagecredit: # URL to image source page, website, or creator
YouTubeID: # The F2SYDXV1W1w part of https://www.youtube.com/watch?v=F2SYDXV1W1w
authorName: You Suck At Cooking 89 # Name of the recipe/article author
authorURL: https://www.youtube.com/watch?v=5ED8PpH0io8 # URL of their home website
sourceName: # Name of the source website
sourceURL: # Actual URL of the recipe itself
category: "dinner" # The type of meal or course your recipe is about. For example: "dinner", "entree", or "dessert".
cuisine: "Italian" # The region associated with your recipe. For example, "French", Mediterranean", or "American".
tags: # You don't have to have 3, feel free to have 10, 1, or none
  - Italian
  - Main
  - Side
  - Bake
  - Vegetarian
  - Long
serves: 6
prepTime: 10
cookTime: 80

ingredients:
- (?)g soy mince
- 500g [tomato sauce](https://food.pauloshea.ie/recipes/tomato-sauce/)
- 1tbsp chilli powder
- 1tbsp cumin
- 2tbsp red wine vinegar
- 450g ricotta
- 1 garlic clove
- 65g grated mozzarella
- lasagna sheets

directions:
- Add about **(?)g soy mince** in boiling water until it's the correct consistency. Let it soak
- Heat **500g [tomato sauce](https://food.pauloshea.ie/recipes/tomato-sauce/)** to a simmer and add **1tbsp chilli powder**, **1tbsp cumin** and **2tbsp red wine vinegar**. Cook for about 10 minutes.
- In a bowl put **450g ricotta**, **1 garlic clove** crushed and **65g grated mozzarella**. Stir until combined
- Preheat the oven at 180C
- Add a very thin layer of sauce to the bottom or a baking dish to stop the pasta sheets from sticking.
- Lasagna assembly time! Add a layer of **lasagna sheets**, then tomato sauce, then ricotta mixture. Repeat until you run out.
- I usualy spread the ricotta on the bottom of pasta sheet layer 2 onwards, it's easier than trying to spread it over the tomato sauce.
- Finish off the lasagna with a thick layer of mozzarella. You're not here to be healthy, go mad.
- Cover the dish with tin foil. Use toothpicks to stop it sticking to the cheese
- Heat for 30-40 minutes in the oven.
- Remove the tinfoil and brown the cheese off under the grill
- Let stand for 20 minutes to stop it slopping everywhere when you cut into it. Enjoy! 

---
