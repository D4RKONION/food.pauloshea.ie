---
layout: recipe
date: 2021-04-03T14:01:03+01:00
draft: false
title: "Chickpea Curry" # The title of your awesome recipe
blurb: This one's taken from the Happy Pear bros and although they originally called this a 5 minute curry, you'd be mad to not let this thing cook for a bit longer to let the flavours develop # A quick description of the recipe
image: chickpea-curry.jpg # Name of image in recipe bundle
imagecredit: # URL to image source page, website, or creator
YouTubeID: # The F2SYDXV1W1w part of https://www.youtube.com/watch?v=F2SYDXV1W1w
authorName: # Name of the recipe/article author
authorURL: # URL of their home website
sourceName: Happy Pear Youtube # Name of the source website
sourceURL: https://www.youtube.com/watch?v=aP6eXpwIths # Actual URL of the recipe itself
category: Dinner # The type of meal or course your recipe is about. For example: "dinner", "entree", or "dessert".
cuisine: Indian # The region associated with your recipe. For example, "French", Mediterranean", or "American".
tags: # You don't have to have 3, feel free to have 10, 1, or none
  - Quick
  - Easy
  - Curry
  - Vegan
  - One Pot
  - Main
  - Spicy
  - Indian
serves: 4
prepTime: 5
cookTime: 35

ingredients:
- Brown rice
- oil
- 1 tin of chickpeas
- red onion
- 1.5 clove of garlic
- ½ thumb of ginger
- 1 red chilli
- 1 tbsp curry powder
- 1 tsp cumin powder
- 1 tsp ground coriander
- 1 tsp ground paprika
- ¼ tsp cayenne
- 1 tin chopped tomatoes
- 1 tin coconut milk
- salt and pepper
- 25g fresh coriander
- lime zest
- ¼ a lime of juice

directions:
- Get your **rice** on. Heat the **oil** on a high heat. 
- Drain **1 tin of chickpeas** and rinse them. 
- Thinly slice a **red onion** and **1.5 clove of garlic**, grate **½ thumb of ginger** and slice **1 red chilli**. Get them all into the pan and cook for 5 minutes on a high heat.
- Add **1 tbsp curry powder**, **1 tsp cumin powder**, **1 tsp ground coriander**, **1 tsp ground paprika**, **¼ tsp cayenne**. Reduce to a medium heat and cook for 2 minutes, stir constantly, add a splash a water if it's sticking
- Add **1 tin chopped tomatoes**, **1 tin coconut milk** and the chickpeas. Add the some **salt and pepper** to taste.
- Bring to a boil and then reduce to a simmer. Cook for about 15-20 minutes, until the consistency is a nice thick curry.
- Chop **25g fresh coriander** (or use dried stuff, who cares) and add to the pan along with some **lime zest**.
- Add **¼ a lime of juice**, season to taste
---
