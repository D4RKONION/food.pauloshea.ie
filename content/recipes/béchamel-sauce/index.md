---
layout: recipe
date: 2021-04-03T12:55:44+01:00
draft: false
title: "⚙️Béchamel Sauce" # The title of your awesome recipe
blurb: I use this White sauce anywhere I need a creamy delicious sauce. It gets tiring typing it out every time, so here it is!  # A quick description of the recipe
image: béchamel-sauce.jpg # Name of image in recipe bundle
imagecredit: https://google.com/ # URL to image source page, website, or creator
YouTubeID: # The F2SYDXV1W1w part of https://www.youtube.com/watch?v=F2SYDXV1W1w
authorName:  # Name of the recipe/article author
authorURL: # URL of their home website
sourceName: # Name of the source website
sourceURL: # Actual URL of the recipe itself
category: Component # The type of meal or course your recipe is about. For example: "dinner", "entree", or "dessert".
cuisine: French  # The region associated with your recipe. For example, "French", Mediterranean", or "American".
tags: # You don't have to have 3, feel free to have 10, 1, or none
  - Component
  - Sauce
  - Short
serves: "?"
prepTime: 0
cookTime: 15

ingredients:
- 50g Flour
- 50g Butter
- 500ml Milk

directions:
- A note on the amounts required. The ratio is always 1:1:10, Flour:Butter:Milk. Some recipes might add milk later to loosen up the sauce that will be noted in those recipes
- Heat a medium sized pan on a medium heat. Add **50g butter**
- Stir in **50g flour** gradually until an even paste. Whisk constantly. At the end you'll have a yellowey paste. This is a roux
- Gradually stir in **500ml milk**, until smooth. It’ll be a little watery, but that's fine, it will thicken up. Resist the urge to add more milk now.
- Bring to a simmer and then cook, whisking to keep it from getting lumpy. Add a little milk if it starts to get stiff or too thick
---
