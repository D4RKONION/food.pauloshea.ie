---
layout: recipe
date: 2021-03-14T17:57:55Z
draft: false
title: "Chilli Non Carne" # The title of your awesome recipe
blurb: "This is what I make when I need a huge batch of food to bring to a pot luck (scaling the quantities obviously!). People always love it but beware: you may need to 1/2 or even 1/8 the chilli powder, cayenne and fresh chilli"
image: chilli.jpg # Name of image in recipe bundle
imagecredit: # URL to image source page, website, or creator
YouTubeID: # The F2SYDXV1W1w part of https://www.youtube.com/watch?v=F2SYDXV1W1w
authorName: # Name of the recipe/article author
authorURL: # URL of their home website
sourceName: # Name of the source website
sourceURL: # Actual URL of the recipe itself
category: "Dinner" # The type of meal or course your recipe is about. For example: "dinner", "entree", or "dessert".
cuisine: "Mexican" # The region associated with your recipe. For example, "French", Mediterranean", or "American".
tags: # You don't have to have 3, feel free to have 10, 1, or none
  - Mexican
  - Chilli
  - Spicy
  - Vegetarian
  - Vegan
  - Pot Luck
  - One Pot
  - Freezes Well
  - Main
serves: 4
prepTime: 10
cookTime: 30

ingredients:
- 60g Soy Mince
- 1 Onion
- 2 Peppers
- 4 Mushrooms
- 3 Cloves Garlic
- 1 Chilli
- Chilli Powder
- Cayenne Powder
- Tomato Puree
- Vege Stock Powder
- 400g Chopped Tomatoes
- Tobasco
- 1 tin Kidney Beans
- Grated Cheddar to serve

directions:
- Soak the **soy mince** in a container (I use a lunch box!). You might need to come back to this to add a bit more water, it should come to the consistency of cooked mince meat
- Chop **1 onion**, **2 peppers** and some **mushrooms**. Finely chop **3 cloves garlic** and **1 chilli** 
- Heat some **oil** in a large pan on a high heat. Add the garlic and chilli for 30 seconds or just browning
- Add the soy mince with **1 tsp chilli powder** and **1 tsp of cayenne powder**. I usually add the powders beforehand to mix it with the mince
- After a few minutes add the onion, peppers, and mushrooms. Cook on same heat for approx. 5 mins.
- Add **1 tbsp tomato puree** and **1 tbsp stock powder** and mix well. Then add **1 tin chopped tomatoes** and about **100ml water** (it might need more water, the liquid should just cover the contents). Add **1 tsp chilli powder**, **1 tsp cayenne powder** and a few dashs of **tabasco**.
- Bring to the boil and then leave to cook uncovered at a low simmer for at least 10 minutes (but the longer the better)
- Drain and rinse a tin of **kidney beans**; add them to the chilli. Cook for at least another 5 minutes on a low simmer
- Serve with rice or nachos, and always lots of grated cheddar!
---
