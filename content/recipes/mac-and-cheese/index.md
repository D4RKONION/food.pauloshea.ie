---
layout: recipe
date: 2022-01-05T21:27:46Z
draft: false
title: "Mac and Cheese" # The title of your awesome recipe
blurb: "An insanely thick and cheesey Mac and Cheese that's more cheese than mac. Prepare yourself. (note: kitty not included)"
image: mac-and-cheese.jpg # Name of image in recipe bundle
imagecredit:  # URL to image source page, website, or creator
YouTubeID: # The F2SYDXV1W1w part of https://www.youtube.com/watch?v=F2SYDXV1W1w
authorName: # Name of the recipe/article author
authorURL:  # URL of their home website
sourceName: YSAC 133 # Name of the source website
sourceURL: https://www.youtube.com/watch?v=Fp2li-mzcyc # Actual URL of the recipe itself
category: Dinner # The type of meal or course your recipe is about. For example: "dinner", "entree", or "dessert".
cuisine: American # The region associated with your recipe. For example, "French", Mediterranean", or "American".
tags: # You don't have to have 3, feel free to have 10, 1, or none
  - Comfort Food
  - American
  - Pasta
  - Roux
  - Vegetarian
  - Quick
serves: 2
prepTime: 0
cookTime: 25

ingredients:
- 35g Flour
- 35g Butter
- 350ml + 150ml Milk
- ¾ tsp Mustard
- ½ tsp Nutritional Yeast Flakes
- ¼ tsp salt
- ¾ tsp Garlic Powder
- ¾ tsp Chilli Powder
- 150g Sharp Cheddar
- Pepper
- 250g Macaroni
- garlic bread
- 2 scallions

directions:
- Make a [Béchamel Sauce](https://food.pauloshea.ie/recipes/béchamel-sauce/) of **35g Flour**, **35g Butter** and **350ml Milk**. We'll add the other 150ml later.
- Once your Béchamel Sauce is made, add **¾ tsp Mustard**, **½ tsp Nutritional Yeast Flakes**, **¼ tsp salt**, **¾ tsp Garlic Powder**, **¾ tsp Chilli Powder**, **Pepper** to taste (1 tsp or so?) and stir it all up
- Bring it to a simmer and cook it for about 10-15 minutes until it's thick. 
- While your sauce thickens, bring **250g Macaroni** to the boil and cook to taste. This is also a good time to put on some **garlic bread**, chop **2 Scallions** (the whole thing) and grate **150g Sharp Cheddar**
- Once your sauce is thick, add 150ml Milk and your Cheddar and stir well
- Drain your pasta and add it to the cheese sauce. Bring the hear way down
- Heat a pan with 1 tbsp olive oil and fry the scallion
- Serve your Mac and Cheese with the scallions and some of the oil from them
- Bonus tips! Mac and Cheese goes AMAZINGLY with Siracha sauce. Squirt some into your bowl and mix it all up!
---
