A modified version of [GoChowdown](https://themes.gohugo.io/gochowdown/)

### Add a new recipe draft

Type `hugo new --kind recipe-bundle recipes/name-of-your-new-recipe-here`

### Add a new recipe with components

Similar to above, but instead of adding the recipes to the `content/recipes` directory, add the individual components to the `content/components` directory. Then add a new recipe as you normally would, and replace the instructions list with a components list, using the title (aka name) of the recipe, and modify the directions section as needed.

## License

Coder is licensed under the [MIT license](https://github.com/seanlane/gochowdown/blob/master/LICENSE.md).
